<?php
get_header();
$args = array(
    's'     => get_search_query(),
    'nopaging'               => false,
    'paged'                  => $paged,
    'post_type' => 'post',
    'posts_per_page' => 5,
    'orderby' => 'post_date',
    'order' => 'DESC'
);

$WPQuery = new WP_Query($args);
?>
    <!-- Topo -->
<?php  get_template_part('components/topo-da-pagina/topo-da-pagina'); ?>

    <section id="blog">

        <div class="container">

            <div class="row">


                <div class="col-md-9 wow fadeIn conteudo-blog">
                    <?php if ($WPQuery->have_posts()) : while ($WPQuery->have_posts()) : $WPQuery->the_post(); ?>

                        <div class="row mb-5">
                            <div class="col-md-5">
                                <a class="link-imagem-post" href="<?php echo get_the_permalink() ?>">
                                    <figure>
                                        <?php the_post_thumbnail('img_post_list', array('alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
                                    </figure>
                                </a>
                            </div>
                            <div class="col-md-7">
                                <a class="link-titulo-post" href="<?php echo get_the_permalink() ?>">
                                    <h2 class="text-left truncate3"><?php echo get_the_title() ?></h2>
                                </a>

                                <p class="truncate3"><?php echo substr(get_the_excerpt(), 0, 125); ?>...</p>
                                <a class="btn btn-primario dark-hover" href="<?php echo get_the_permalink() ?>">Leia
                                    Mais</a>

                            </div>
                        </div>
                    <?php endwhile; endif; ?>
                    <div class="practice-v3-paginat text-center">
                        <?php echo bootstrap_pagination($WPQuery); ?>
                    </div>
                </div>

                <div class="col-md-3 sidebar-blog">
                    <?php dynamic_sidebar('blog_sidebar'); ?>
                </div>
            </div>

        </div>

    </section>
<?php /* Restore original Post Data */
wp_reset_postdata();
get_footer();?>