<?php 
// Enable post thumbnails
add_theme_support('post-thumbnails');

// Custom image sizes
if (function_exists('add_image_size')) {
  add_image_size('logo', 250, 74, true); // Logo
  add_image_size('slides', 1920, 720); // Slides
  add_image_size('col_6', 960, 630, true); // Coluna (Metade)
  add_image_size('cta_home', 1920, 385, true); // CTA Home
  add_image_size('obras_home', 590, 350, true); // Obras Home
  add_image_size('topo_da_pagina', 1920, 320, true); // Topo da Página
  add_image_size('img_full', 145, 145); // Imagem Fullwidth
    add_image_size('nossos_valores', 145, 145); // Nossos Valores


}