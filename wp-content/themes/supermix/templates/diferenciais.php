<?php
/* Template Name: Diferenciais */
get_header();
$imagemDiferencias = get_field('imagem_diferenciais')
?>
    <!-- Topo -->
<?php get_template_part('components/topo-da-pagina/topo-da-pagina'); ?>

    <section id="diferenciais">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </section>

    <!-- Locais -->
<?php get_template_part('components/diferenciais/locais'); ?>



    <!-- Imagem 2 -->
    <section id="img-diferenciais">
        <img src="<?php print_r($imagemDiferencias['sizes']['img_full']) ?>"
             alt="<?php the_title() ?>" title="<?php the_title() ?>" class="img-diferenciais">

    </section>

    <!-- Onde Estamos -->
<?php get_template_part('components/onde-estamos/onde-estamos'); ?>

    <!-- Call to Action -->
<?php get_template_part('components/call-to-action/cta'); ?>

<?php get_footer(); ?>