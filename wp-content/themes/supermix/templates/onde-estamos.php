<?php
/* Template Name: Onde Estamos */
get_header();
$grupoOndeEstamos = get_field('grupo_onde_estamos');

?>

    <!-- Topo -->
<?php get_template_part('components/topo-da-pagina/topo-da-pagina'); ?>
    <section id="solucao" style="margin-bottom: 100px">
        <div class="row">
            <div class="col-md-6 solucao-content">
                <h2 class="titulo-destaque duplicate text-center text-md-left mb-4"
                    title="<?php echo $grupoOndeEstamos['titulo'] ?>"><?php echo $grupoOndeEstamos['titulo'] ?></h2>
                <div class="text-md-left">
                    <?php echo $grupoOndeEstamos['texto'] ?>
                </div>
            </div>
            <div class="col-md-6">
                <img style="width: 100%" class="img-solucao"
                     src="<?php echo wp_get_attachment_image_url($grupoOndeEstamos['imagem'], 'vantagens'); ?>"
                     alt="" class="mb-4">
            </div>
        </div>
    </section>

    <section id="select-localidades" style="margin: 150px 0;">
        <img class="fundo-onde-estamos" src="<?php echo get_template_directory_uri() . '/assets/img/fundo-coracao.png'; ?>" alt="">

        <div class="container">
            <form action="">
            <div class="row">

                    <div class="col-md-6">
                        <div class="form-group mb-1 pb-0">
                            <select name="estado" class="form-control" id="estado" aria-invalid="false">
                                <option value="Selecione o Estado" selected disabled>Selecione o Estado</option>
                                <?php
                                $terms = get_terms(
                                    array(
                                        'taxonomy'   => 'estados',
                                        'hide_empty' => true,
                                        'orderby' => 'title',
                                        'order' => 'ASC'
                                    )
                                );

                                // Check if any term exists
                                if ( ! empty( $terms ) && is_array( $terms ) ) {
                                    // Run a loop and print them all
                                    foreach ( $terms as $term ) { ?>
                                        <option class="<?php echo $term->slug; ?>" value="<?php echo $term->name; ?>"><?php echo $term->name; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <?php
                        $terms = get_terms(
                            array(
                                'taxonomy'   => 'estados',
                                'hide_empty' => true,
                                'orderby' => 'title',
                                'order' => 'ASC'
                            )
                        );

                        // Check if any term exists
                        if ( ! empty( $terms ) && is_array( $terms ) ) {
                            // Run a loop and print them all
                            foreach ( $terms as $term ) { ?>
                        <select style="display: none;" onChange="window.location.href=this.value" name="<?php echo $term->slug; ?>" class="form-control municipio" id="<?php echo $term->slug; ?>" aria-invalid="false">
                            <option value="Selecione o Município" selected disabled>Selecione o Município</option>
                            <?php
                            $args = array(
                                'posts_per_page' => -1,
                                'post_type' => 'localidades',
                                'orderby' => 'title',
                                'order' => 'ASC',
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'estados',
                                        'field' => 'slug',
                                        'terms' => $term->slug,
                                    ),
                                ),
                            );
                            $WPquery = new WP_Query( $args );
                            ?>

                            <?php if ($WPquery->have_posts()) : while ($WPquery->have_posts()) : $WPquery->the_post(); ?>
                                <option value="<?php echo get_the_permalink() ?>"><?php the_title() ?></option>

                            <?php endwhile; endif; ?>
                            <?php /* Restore original Post Data */
                            wp_reset_postdata(); ?>
                        </select>
                                <?php
                            }
                        }
                        ?>
                    </div>
            </div>
            </form>
        </div>
    </section>


    <!-- Call to Action -->
<?php get_template_part('components/call-to-action/cta'); ?>

<?php get_footer(); ?>