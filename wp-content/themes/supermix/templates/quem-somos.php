<?php
/* Template Name: Quem Somos */
get_header();
$imagem1 = get_field('imagem_1');
$imagem2 = get_field('imagem_2');
$codigoDeConduta = get_field('codigo_de_conduta');
?>
    <!-- Topo -->
<?php get_template_part('components/topo-da-pagina/topo-da-pagina'); ?>

    <section id="a-supermix">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </section>

    <!-- Imagem 2 -->
    <section id="img-1">
        <img src="<?php print_r($imagem1['sizes']['img_full']) ?>"
             alt="<?php the_title() ?>" title="<?php the_title() ?>" class="img-1">

    </section>

    <!-- Nossos Valores -->
<?php get_template_part('components/a-supermix/nossos-valores'); ?>

    <!-- Imagem 2 -->
    <section id="img-2">
        <img src="<?php print_r($imagem2['sizes']['img_full']) ?>"
             alt="<?php the_title() ?>" title="<?php the_title() ?>" class="img-2">

    </section>

    <section id="codigo-de-conduta">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="titulo mb-5">
                        <?php echo $codigoDeConduta['titulo'] ?>
                    </h2>
                    <?php echo $codigoDeConduta['texto'] ?>
                </div>
            </div>
        </div>
    </section>

    <!-- Onde Estamos -->
<?php get_template_part('components/onde-estamos/onde-estamos'); ?>

    <!-- Call to Action -->
<?php get_template_part('components/call-to-action/cta'); ?>

<?php get_footer(); ?>