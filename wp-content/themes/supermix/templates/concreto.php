<?php
/* Template Name: Concreto */
get_header();
$imagemDiferencias = get_field('imagem_diferenciais');
$banner = get_field('banner')
?>
    <!-- Topo -->
<?php get_template_part('components/topo-da-pagina/topo-da-pagina'); ?>

    <section id="nossos-produtos">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 conteudo"
                     style="background-image: url(<?php echo get_template_directory_uri() . '/assets/img/background-supermix.jpg'; ?>)">
                    <span class="traco"></span>

                    <h2 class="mb-5"><?php echo get_field('titulo_produtos') ?></h2>
                    <?php the_content(); ?>
                    <a href="<?php echo $produtos['link_do_botao'] ?>"
                       class="fw-medium"><?php echo $produtos['texto_do_botao'] ?><span class="undeline"></span></a>
                </div>

                <div class="col-md-6 p-0">
                    <img src="<?php print_r($banner['imagem']['sizes']['col_6']) ?>"
                         alt="<?php the_title() ?>" title="<?php the_title() ?>" class="img-banner-nossos-produtos">
                    <div class="banner-content">
                        <h4 class="fw-bold text-center"><?php echo $banner['titulo'] ?></h4>
                        <h5 class="fw-bold text-center"><?php echo $banner['subtitulo'] ?></h5>
                        <a href="#blocks" class="view-products"><i class="fas fa-chevron-down"></i></a>
                    </div>

                </div>
            </div>
        </div>

    </section>


    <!-- Blocos -->
<?php get_template_part('components/produtos/blocks'); ?>

    <!-- Onde Estamos -->
<?php get_template_part('components/onde-estamos/onde-estamos'); ?>

    <!-- Call to Action -->
<?php get_template_part('components/call-to-action/cta'); ?>

<?php get_footer(); ?>