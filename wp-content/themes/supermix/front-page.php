<?php get_header() ?>

<!-- Slide -->
<?php  get_template_part('components/slide-bootstrap/slide'); ?>

<!--Porque a Supermix-->
<?php  get_template_part('components/index/porque-a-supermix'); ?>

<!-- Produtos -->
<?php  get_template_part('components/index/produtos'); ?>

<!-- Call to Action Home -->
<?php  get_template_part('components/index/cta'); ?>

<!-- Obras Realizadas -->
<?php  get_template_part('components/index/obras-realizadas'); ?>

<!-- Onde Estamos -->
<?php  get_template_part('components/onde-estamos/onde-estamos'); ?>

<!-- Call to Action -->
<?php  get_template_part('components/call-to-action/cta'); ?>

<?php get_footer() ?>