<?php
$grupoFooter = get_fields('options')['grupo_footer'];
$telefones = get_field('grupo_informacoes_para_contato', 'options')['telefones'];
$emails = get_field('grupo_informacoes_para_contato', 'options')['emails'];
$enderecos = get_field('grupo_informacoes_para_contato', 'options')['enderecos'];
$whatsapps = get_field('grupo_informacoes_para_contato', 'options')['whatsapp'];
?>

<!-- Footer -->
<footer class="font-small text-white">

    <!-- Footer Links -->
    <div class="container text-center text-sm-left wrapper-footer">

        <!-- Grid row -->
        <div class="row ">

            <div class="col-md-3 logo-footer">
                <a class="logo-footer mb-3" href="<?php bloginfo('url'); ?>">
                    <?php echo wp_get_attachment_image(get_field('grupo_header', 'options')['logo_colorida'], 'logo'); ?>
                </a>

            </div>

            <div class="col-md-3 informacoes mt-4 mt-md-0">
                <h4 class="text-white fw-bold">Lorem Ipsum</h4>

                <p class="texto-footer">
                    <?php echo $grupoFooter['Texto_footer'] ?>
                </p>
                <?php foreach ($emails as $email) : ?>
                    <span class="email mr-2 mb-2">
                    <i class="fas fa-envelope mr-2 "></i>
                    <a href="mailto:<?php echo $email['endereco_email']; ?>" target="_blank">
                    <?php echo $email['endereco_email']; ?>
                    </a>
                </span>
                <?php endforeach; ?>

                <?php foreach ($telefones as $telefone) : ?>
                    <span class="telefone mr-2 mb-2">
                    <i class="fas fa-phone-alt mr-2 "></i>
                    <a href="tel:<?php echo $telefone['numero_telefone']; ?>" target="_blank">
                    <?php echo $telefone['numero_telefone']; ?>
                    </a>
                </span>
                <?php endforeach; ?>

                <?php foreach ($whatsapps as $whatsapp) : ?>
                    <span class="whatsapp mr-2 mb-2">
                    <i class="fab fa-whatsapp mr-2 "></i>
                    <a href="https://api.whatsapp.com/send?phone=55<?php echo $whatsapp['link_whatsapp']; ?>&text=Ola,%20tudo%20bem?"
                       target="_blank">
                    <?php echo $whatsapp['numero_whatsapp']; ?>
                    </a>
                </span>
                <?php endforeach; ?>


            </div>

            <div class="col-md-3 links-uteis mt-4 mt-md-0">
                <h4 class="text-white fw-bold">Lorem Ipsum</h4>
                <?php $localizacao = get_nav_menu_locations(); ?>
                <?php $menu_obj = get_term($localizacao['secondary']); ?>
                <?php $nome_menu = $menu_obj->name; ?>
                <?php wp_nav_menu(array('theme_location' => 'secondary')); ?>

            </div>

            <div class="col-md-3 links-uteis mt-4 mt-md-0">
                <h4 class="text-white fw-bold">Lorem Ipsum</h4>
                <?php $localizacao = get_nav_menu_locations(); ?>
                <?php $menu_obj = get_term($localizacao['secondary']); ?>
                <?php $nome_menu = $menu_obj->name; ?>
                <?php wp_nav_menu(array('theme_location' => 'secondary')); ?>

            </div>

        </div><!-- row -->

    </div><!-- container -->
    <!-- Footer Links -->

    <!-- Copyright -->
    <?php get_template_part('/components/footer/cia-logo-footer'); ?>
    <!-- Copyright -->

</footer>
<div class="mobile-menu-overlay"></div>
<!-- Footer -->
<div class="botao-lateral-wrapper">
    <div class="botao-lateral">
        <a title="Contato" href="#" rel="nofollow noreferrer noopener external" data-toggle="modal" data-target="#exampleModal">
            <img src="<?php echo get_template_directory_uri() . '/assets/img/envelope-btn-lateral.png'; ?>"
                 alt="<?php the_title() ?>" class="img-btn-lateral">
        </a>
    </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-primario fw-bold text-center w-100">Fale Conosco</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="wrapper-form-modal bg-secundario p-3 pt-5">
                <?php  get_template_part('components/formularios/contact'); ?>
            </div>
        </div>
    </div>
</div>
<?php wp_footer() ?>

</body>

</html>