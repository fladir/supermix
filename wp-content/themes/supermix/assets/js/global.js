(function ($) {
    // Select Dinâmico Pág. Onde Estamos
    $('#estado').on('change', function () {
        var todosMunicipios = $('.municipio');
        var estados = $("#estado option:selected").attr('class');

        $(todosMunicipios).hide();
        $('#' + estados).show();
    });

    $(document).ready(function () {
        // Depoimentos
        $(".produtos-carousel").owlCarousel({
            navigation: true,
            loop: true,
            paginationSpeed: 400,
            autoplay: true,
            autoplayHoverPause: true,
            smartSpeed: 700,
            autoplayTimeout: 7000,
            responsiveClass: true,
            slideBy: 1,
            items: 1,
            dots: false,
            nav: true,
            navText: ['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>']
        });

        //WOW Effect
        wow = new WOW(
            {
                boxClass: 'wow',      // default
                animateClass: 'animated', // default
                offset: 0,          // default
                mobile: true,       // default
                live: true        // default
            }
        );
        wow.init();
    });

    //Botão Menu Mobile
    $('.nav-toggler, .mobile-menu-overlay').click(function () {
        $('.nav-toggler').toggleClass('toggler-open');
        $('.mobile-menu-wrapper, .mobile-menu-overlay').toggleClass('open');
        $('body, #menu-secundario').toggleClass('arredar');
    });

    //Contagem Nossos Números
    $('#nossos-numeros').waypoint(function () {
        $('.count').each(function () {
            $(this).prop('Counter', 0).animate({
                Counter: $(this).text()
            }, {
                duration: 3000,
                easing: 'easeOutExpo',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
        $('.numeros').addClass('animated fadeIn faster');
        this.destroy()
    }, {offset: '50%'});

    $('.nav-link').removeAttr('title');

// Botão voltar ao topo
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('#voltarTopo').addClass('show')
        } else {
            $('#voltarTopo').removeClass('show');
        }
    });
    $('#voltarTopo').click(function () {
        $("html, body").animate({scrollTop: 0}, 1200, 'easeInOutExpo');
        return false;
    });

    $(".view-products").click(function(event) {
        event.preventDefault();
        $('html,body').animate({
                scrollTop: $("#blocks").offset().top},
            1200, 'easeInOutExpo');
    });

})(jQuery);
