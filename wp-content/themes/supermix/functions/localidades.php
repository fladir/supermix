<?php
add_action('init', 'type_post_localidades');

function type_post_localidades() {
    $labels = array(
        'name' => _x('Localidades', 'post type general name'),
        'singular_name' => _x('Localidade', 'post type singular name'),
        'add_new' => _x('Adicionar Nova Localidade', 'Nova Localidade'),
        'add_new_item' => __('Nova Localidade'),
        'edit_item' => __('Editar Localidade'),
        'new_item' => __('Nova Localidade'),
        'view_item' => __('Ver Localidade'),
        'search_items' => __('Procurar Localidades'),
        'not_found' =>  __('Nenhum registro encontrado'),
        'not_found_in_trash' => __('Nenhum registro encontrado na lixeira'),
        'parent_item_colon' => '',
        'menu_name' => 'Localidades'
    );


    $args = array(
        'labels' => $labels,
        'public' => true,
        'public_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'register_meta_box_cb' => 'portfolio_meta_box',
        'menu_icon' => 'dashicons-admin-site-alt',
        'supports' => array('title', 'editor', 'thumbnail'),
    );

    register_post_type( 'localidades' , $args );
    flush_rewrite_rules();
}

function themes_taxonomy() {
    register_taxonomy(
        'estados',  // The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
        'localidades',             // post type name
        array(
            'hierarchical' => true,
            'label' => 'Estados', // display name
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'estado',    // This controls the base slug that will display before each term
                'with_front' => false  // Don't display the category base before
            )
        )
    );
}
add_action( 'init', 'themes_taxonomy');
?>