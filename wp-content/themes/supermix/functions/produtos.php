<?php 

#PRODUTO
$args = array(
    'supports'  => array('title', 'editor', 'thumbnail', 'excerpt'),
    'menu_icon'           => 'dashicons-image-filter',
  );
$custom_post_type_slide = new pbo_register_custom_post_type('produto', 'Produto', $args);

$iniciar_taxonomia_produtos = new pbo_register_custom_taxonomy('categoria-produto', 'Categoria Produtos', 'produto', $args);