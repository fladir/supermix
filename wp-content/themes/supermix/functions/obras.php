<?php 

#OBRA
$args = array(
    'supports'  => array('title', 'editor', 'thumbnail', 'excerpt'),
    'menu_icon'           => 'dashicons-building',
  );
$custom_post_type_obra = new pbo_register_custom_post_type('obra', 'Obra', $args);

$iniciar_taxonomia_obra = new pbo_register_custom_taxonomy('categoria-obra', 'Categoria Obras', 'obra', $args);