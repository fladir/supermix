<?php


require get_template_directory() . "/core/vendor/PHPMailer/Exception.php";
require get_template_directory() . "/core/vendor/PHPMailer/OAuth.php";
require get_template_directory() . "/core/vendor/PHPMailer/PHPMailer.php";
require get_template_directory() . "/core/vendor/PHPMailer/POP3.php";
require get_template_directory() . "/core/vendor/PHPMailer/SMTP.php";

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class MontarMensagem
{
    private $nome = null;
    private $email = null;
    private $mensagem = null;
    public $status = array('codigo_status' => 0, 'descricao_status' => '');
    
    public function __get($atributo)
    {
        return $this->$atributo;
    }
    public function __set($atributo, $valor)
    {
        $this->$atributo = $valor;
    }
    
}


function enviarMensagemSimples()
{
    #MONTANDO DADOS DA MENSAGEM
    $mensagem = new MontarMensagem();
    $mensagem->__set('nome', $_POST['nome']);
    $mensagem->__set('email', $_POST['email']);
    $mensagem->__set('mensagem', $_POST['mensagem']);   
    
    #PEGANDO DADOS SMTP
    $optionsACF = get_fields('options')['grupo_de_configuracoes_gerais'] ;
    $smtp = $optionsACF['config_smtp'];
    
    $destinatario = $smtp['mail_do_destinatario'];
    $host = $smtp['host'];
    $usuario = $smtp['usuario_smtp'];
    $senha = $smtp['senha_smtp'];    
    
    $logoUrl = get_field('grupo_header','options')['logo_email'];
    
    $siteCliente = get_home_url();   
    
    
    #PEGANDO TEMPLATE DE EMAIL
    require get_template_directory() . '/mail/contato-simples/email.php';
    
    #INSTANCIANDO PHPMAILER E ENVIANDO EMAIL
    $mail = new PHPMailer(true);
    
    try {
        //Server settings
        $mail->SMTPDebug = false; #SMTP::DEBUG_SERVER;                      // Enable verbose debug output
        $mail->isSMTP();                                                   // Send using SMTP
        $mail->CharSet    = 'UTF-8';                                       // Charset do form
        $mail->Host       = $host;                                        // Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                        // Enable SMTP authentication
        $mail->Username   = $usuario;                                   // SMTP username
        $mail->Password   = $senha;                                    // SMTP password
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;           // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
        $mail->Port       = 587;                                     // TCP port to connect to
        
        //Recipients
        $mail->setFrom($usuario);
        $mail->addAddress($destinatario);                               // Add a recipient
        # $mail->addAddress('ellen@example.com');                 // Name is optional
        # $mail->addReplyTo('cc@example.com', 'Informações');
        # $mail->addCC('cc@example.com');
        # $mail->addBCC('bcc@example.com');
        
        // Attachments
        # $mail->addAttachment('/var/tmp/file.tar.gz');       // Add attachments
        # $mail->addAttachment('/tmp/image.jpg', 'new.jpg'); // Optional name
        
        // Content
        $mail->isHTML(true);                              // Set email format to HTML
        $mail->Subject = "OPA! Chegou um novo contato.";
        
        $mail->Body = $mensagemHTML;
        $mail->AltBody = 'Seu navegador não suporta a mensagem enviada, é necessário acessar este email por outro navegador.';
        
        $mail->send();
        
        $mensagem->status['codigo_status'] = 200;
        $mensagem->status['codigo_descricao'] = 'E-mail enviado com sucesso';
    } catch (Exception $e) {
        $mensagem->status['codigo_status'] = 404;
        $mensagem->status['codigo_descricao'] = 'Não foi possível enviar esta mensagem! Por favor tente novamente. Detalhes do erro: ' . $mail->ErrorInfo;
    }    
    
    $urlPaginaResposta = $siteCliente . '/mensagem-enviada';
    
    $resposta = array(
        'status' => $mensagem->status['codigo_status'],
        'descricao' => $mensagem->status['codigo_descricao'],
        'url' => $urlPaginaResposta,
    );
    echo json_encode($resposta);    
    exit;
}
#AJAX Contato
add_action('wp_ajax_enviarMensagemSimples', 'enviarMensagemSimples');
add_action('wp_ajax_nopriv_enviarMensagemSimples', 'enviarMensagemSimples');


// Mensagem página Orçamento
function enviarMensagem()
{
    #MONTANDO DADOS DA MENSAGEM
    $mensagem = new MontarMensagem();
    $mensagem->__set('nome', $_POST['nome']);
    $mensagem->__set('tel', $_POST['tel']);
    $mensagem->__set('email', $_POST['email']);
    $mensagem->__set('bairro', $_POST['bairro']);
    $mensagem->__set('cidade', $_POST['cidade']);
    $mensagem->__set('mensagem', $_POST['mensagem']);   
    
    #PEGANDO DADOS SMTP
    $optionsACF = get_fields('options')['grupo_de_configuracoes_gerais'] ;
    $smtp = $optionsACF['config_smtp'];
    
    $destinatario = $smtp['mail_do_destinatario'];
    $host = $smtp['host'];
    $usuario = $smtp['usuario_smtp'];
    $senha = $smtp['senha_smtp'];    
    
    $logoUrl = get_field('grupo_header','options')['logo_email'];
    
    $siteCliente = get_home_url();   
    
    
    #PEGANDO TEMPLATE DE EMAIL
    require get_template_directory() . '/mail/contato/email.php';
    
    #INSTANCIANDO PHPMAILER E ENVIANDO EMAIL
    $mail = new PHPMailer(true);
    
    try {
        //Server settings
        $mail->SMTPDebug = false; #SMTP::DEBUG_SERVER;                      // Enable verbose debug output
        $mail->isSMTP();                                                   // Send using SMTP
        $mail->CharSet    = 'UTF-8';                                      // Charset do form
        $mail->Host       = $host;                                        // Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                        // Enable SMTP authentication
        $mail->Username   = $usuario;                                   // SMTP username
        $mail->Password   = $senha;                                    // SMTP password
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;           // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
        $mail->Port       = 587;                                     // TCP port to connect to
        
        //Recipients
        $mail->setFrom($usuario);
        $mail->addAddress($destinatario);                               // Add a recipient
        # $mail->addAddress('ellen@example.com');                 // Name is optional
        # $mail->addReplyTo('cc@example.com', 'Informações');
        # $mail->addCC('cc@example.com');
        # $mail->addBCC('bcc@example.com');
        
        // Attachments
        # $mail->addAttachment('/var/tmp/file.tar.gz');       // Add attachments
        # $mail->addAttachment('/tmp/image.jpg', 'new.jpg'); // Optional name
        
        // Content
        $mail->isHTML(true);                              // Set email format to HTML
        $mail->Subject = "Novo contato - Página de orçamento";
        
        $mail->Body = $mensagemHTML;
        $mail->AltBody = 'Seu navegador não suporta a mensagem enviada, é necessário acessar este email por outro navegador.';
        
        $mail->send();
        
        $mensagem->status['codigo_status'] = 200;
        $mensagem->status['codigo_descricao'] = 'E-mail enviado com sucesso';
    } catch (Exception $e) {
        $mensagem->status['codigo_status'] = 404;
        $mensagem->status['codigo_descricao'] = 'Não foi possível enviar esta mensagem! Por favor tente novamente. Detalhes do erro: ' . $mail->ErrorInfo;
    }    
    
    $urlPaginaResposta = $siteCliente . '/mensagem-enviada';
    
    $resposta = array(
        'status' => $mensagem->status['codigo_status'],
        'descricao' => $mensagem->status['codigo_descricao'],
        'url' => $urlPaginaResposta,
    );
    echo json_encode($resposta);    
    exit;
}
#AJAX Contato
add_action('wp_ajax_enviarMensagem', 'enviarMensagem');
add_action('wp_ajax_nopriv_enviarMensagem', 'enviarMensagem');

// Mensagem do formulário Profissional
function enviarMensagemProfissional()
{
    #MONTANDO DADOS DA MENSAGEM
    $mensagem = new MontarMensagem();
    $mensagem->__set('nome', $_POST['nome']);
    $mensagem->__set('aniversario', $_POST['aniversario']);
    $mensagem->__set('tel', $_POST['tel']);
    $mensagem->__set('cel', $_POST['cel']);
    $mensagem->__set('email', $_POST['email']);

    $mensagem->__set('endereco', $_POST['endereco']);
    $mensagem->__set('bairro', $_POST['bairro']);
    $mensagem->__set('cidade', $_POST['cidade']);
    $mensagem->__set('estado', $_POST['estado']);

    $mensagem->__set('atividade', $_POST['atividade']);
    $mensagem->__set('mensagemAtividade', $_POST['mensagemAtividade']);

    $mensagem->__set('contaBancaria', $_POST['conta']);
      
    $mensagem->__set('mensagemAdicional', $_POST['mensagem']);  

    
    #PEGANDO DADOS SMTP
    $optionsACF = get_fields('options')['grupo_de_configuracoes_gerais'] ;
    $smtp = $optionsACF['config_smtp'];
    
    $destinatario = $smtp['mail_do_destinatario'];
    $host = $smtp['host'];
    $usuario = $smtp['usuario_smtp'];
    $senha = $smtp['senha_smtp'];    
    
    $logoUrl = get_field('grupo_header','options')['logo_email'];
    
    $siteCliente = get_home_url();   
    
    
    #PEGANDO TEMPLATE DE EMAIL
    require get_template_directory() . '/mail/contato-profissional/email.php';
    
    #INSTANCIANDO PHPMAILER E ENVIANDO EMAIL
    $mail = new PHPMailer(true);
    
    try {
        //Server settings
        $mail->SMTPDebug = false; #SMTP::DEBUG_SERVER;                      // Enable verbose debug output
        $mail->isSMTP();                                                   // Send using SMTP
        $mail->CharSet    = 'UTF-8';                                      // Charset do form
        $mail->Host       = $host;                                        // Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                        // Enable SMTP authentication
        $mail->Username   = $usuario;                                   // SMTP username
        $mail->Password   = $senha;                                    // SMTP password
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;           // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
        $mail->Port       = 587;                                     // TCP port to connect to
        
        //Recipients
        $mail->setFrom($usuario);
        $mail->addAddress($destinatario);                               // Add a recipient
        # $mail->addAddress('ellen@example.com');                 // Name is optional
        # $mail->addReplyTo('cc@example.com', 'Informações');
        # $mail->addCC('cc@example.com');
        # $mail->addBCC('bcc@example.com');
        
        // Attachments
        # $mail->addAttachment('/var/tmp/file.tar.gz');       // Add attachments
        # $mail->addAttachment('/tmp/image.jpg', 'new.jpg'); // Optional name
        
        // Content
        $mail->isHTML(true);                              // Set email format to HTML
        $mail->Subject = "Novo contato Profissional!";
        
        $mail->Body = $mensagemHTML;
        $mail->AltBody = 'Seu navegador não suporta a mensagem enviada, é necessário acessar este email por outro navegador.';
        
        $mail->send();
        
        $mensagem->status['codigo_status'] = 200;
        $mensagem->status['codigo_descricao'] = 'E-mail enviado com sucesso';
    } catch (Exception $e) {
        $mensagem->status['codigo_status'] = 404;
        $mensagem->status['codigo_descricao'] = 'Não foi possível enviar esta mensagem! Por favor tente novamente. Detalhes do erro: ' . $mail->ErrorInfo;
    }    
    
    $urlPaginaResposta = $siteCliente . '/mensagem-enviada';
    
    $resposta = array(
        'status' => $mensagem->status['codigo_status'],
        'descricao' => $mensagem->status['codigo_descricao'],
        'url' => $urlPaginaResposta,
    );
    echo json_encode($resposta);    
    exit;
}
#AJAX Contato
add_action('wp_ajax_enviarMensagemProfissional', 'enviarMensagemProfissional');
add_action('wp_ajax_nopriv_enviarMensagemProfissional', 'enviarMensagemProfissional');