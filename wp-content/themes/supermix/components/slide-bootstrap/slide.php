<?php
$slides = get_field('grupo_conteudos_dos_componentes', 'options')['slide_repetidor'];
?>
<?php if ($slides) : ?>
    <section class="slides">
        <div id="slide-bootstrap" class="carousel slide" data-ride="carousel" data-interval="8000">

            <ol class="carousel-indicators justify-content-center">

                <?php foreach ($slides as $key => $slide) : ?>

                    <li data-target="#slide-bootstrap" data-slide-to="<?php echo $key; ?>"
                        class="<?php echo $key == 0 ? "active" : "" ?>"></li>

                <?php endforeach; ?>

            </ol>

            <div class="carousel-inner">

                <?php foreach ($slides as $key => $slide) : ?>
                    <div class="carousel-item <?php echo $key == 0 ? 'active' : '' ?>">
                        <!-- Imagem -->
                        <?php echo wp_get_attachment_image($slide['imagem'], 'slides', '', array('alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>

                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2 class="text-white animated fadeInDown"><?php echo $slide['titulo'] ?></h2>
                                    <?php if ($slide['link_botao']) : ?>
                                        <a class="btn btn-primario animated fadeInUp mt-0 mt-md-4" href="<?php echo $slide['link_botao']; ?>"
                                           target="_blank">
                                            <?php echo $slide['texto_botao']; ?>
                                        </a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>

            </div>
        </div>
    </section>
<?php endif; ?>