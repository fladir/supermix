<?php
$grupoCTA = get_field('grupo_conteudos_dos_componentes', 'options')['grupo_cta'];
?>
<section id="call-to-action">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-12">
                <img src="<?php echo get_template_directory_uri() . '/assets/img/bg-cta.png'; ?>"
                     alt="<?php the_title() ?>" class="bg-cta">
                <div class="conteudo text-center">
                    <h2 class="text-center">
                        <?php echo $grupoCTA['chamada'] ?>
                    </h2>
                    <a href="<?php echo $grupoCTA['link_do_botao'] ?>" class="btn btn-primario mt-2">
                        <?php echo $grupoCTA['texto_do_botao'] ?>
                    </a>
                </div>

            </div>
        </div>
    </div>
</section>