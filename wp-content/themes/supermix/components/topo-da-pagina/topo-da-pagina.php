<?php
$grupoTopoDaPaginaGeral = get_field('grupo_conteudos_dos_componentes', 'options')['imagem_de_fundo'];
?>

<section id="topo-da-pagina">
    <img src="<?php print_r($grupoTopoDaPaginaGeral['sizes']['topo_da_pagina']) ?>"
         alt="<?php the_title() ?>" title="<?php the_title() ?>" class="bg-topo-da-pagina">

    <div class="container">
        <div class="row d-flex align-items-end">

            <div class="col-md-6 text-left">
                <h1 class="text-primario fw-bold"><?php the_title() ?></h1>
            </div>
            <div class="col-md-6 text-right">
                <?php
                if (function_exists('yoast_breadcrumb')) {
                    yoast_breadcrumb('<p id="breadcrumbs">', '</p>');
                }
                ?>
            </div>
        </div>
    </div>
</section>
