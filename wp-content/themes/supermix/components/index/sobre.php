<?php $sobre = get_field('grupo_conteudos_dos_componentes', 'options')['quem_somos']; ?>
<section class="bianca-sobre <?php if (is_page('home') || is_front_page()) { echo 'bg-infinito'; } ?> wow fadeInUp">
    <div class="container">
        <div class="row">
            <h2 class="titulo-destaque"><?php echo $sobre['titulo']; ?></h2>
            <div class="col-12 col-sm-10 offset-sm-1 col-lg-8 offset-lg-2">
                <div class="row">
                    <div class="col-4">
                        <figure>
                            <?php echo wp_get_attachment_image($sobre['imagem'], 'sobre-index', array('alt' => '' . get_the_title() . '', 'title' =>  '' . get_the_title() . '')); ?>
                        </figure>
                    </div>

                    <div class="col-8">
                        <p><?php echo $sobre['texto']; ?></p>
                        <?php if (is_page('home') || is_front_page()) : ?>
                            <?php if ($sobre['link_botao']) : ?>
                                <a href="<?php echo $sobre['link_botao']; ?>"><?php echo $sobre['texto_botao'] ?></a>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>