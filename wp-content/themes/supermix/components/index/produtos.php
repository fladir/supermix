<?php
$produtos = get_field('produtos');
$args = array(
    'nopaging' => false,
    'post_type' => 'produto',
    'posts_per_page' => 8,
    'order' => 'DESC'
);
$WPQuery = new WP_Query($args);
#echo '<pre>'; print_r($produtos); echo '</pre>';
?>

<section id="produtos">

    <div class="row">
        <div class="col-md-6 conteudo"
             style="background-image: url(<?php echo get_template_directory_uri() . '/assets/img/background-supermix.jpg'; ?>)">
            <span class="traco"></span>

            <h2 class="mb-5"><?php echo $produtos['titulo'] ?></h2>
            <?php echo $produtos['texto'] ?>
            <a href="<?php echo $produtos['link_do_botao'] ?>"
               class="fw-medium"><?php echo $produtos['texto_do_botao'] ?><span class="undeline"></span></a>
        </div>

        <div class="col-md-6 p-0">
            <div class="carousel-produtos-overlay"></div>

            <div class="owl-carousel owl-theme produtos-carousel">
                <?php if ($WPQuery->have_posts()) : while ($WPQuery->have_posts()) : $WPQuery->the_post(); ?>
                    <div class="item">

                        <?php the_post_thumbnail('col_6', array('class' => 'img-produtos', 'alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>

                        <div class="carousel-content">
                            <h3 class="fw-bold mb-4"><?php the_title() ?></h3>
                            <?php the_excerpt(); ?>
                        </div>
                    </div>

                <?php endwhile; endif; wp_reset_postdata(); ?>
            </div>
        </div>
    </div>

</section>
