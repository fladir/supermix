<?php
$produtos = get_field('produtos');
$args = array(
    'nopaging' => false,
    'post_type' => 'obra',
    'posts_per_page' => 3,
    'order' => 'ASC'
);
$WPQuery = new WP_Query($args);

?>

<section id="obras-realizadas">
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex justify-content-center flex-column pb-5 mb-3">
                <span class="traco"></span>
                <h2 class="text-center titulo">Obras <strong>Realizadas</strong></h2>
            </div>
        </div>
        <?php
        $i = 0;
        if ($WPQuery->have_posts()) : while ($WPQuery->have_posts()) : $WPQuery->the_post();
            $class = $i % 2 ? 'flex-row-reverse' : '';
            ?>
            <div class="row <?php echo $class; ?>">
                <div class="col-md-6 p-0">
                    <?php the_post_thumbnail('obras_home', array('class' => 'img-obras-realizadas', 'alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>

                </div>
                <div class="col-md-6 p-0">
                    <div class="conteudo">
                        <h3 class="fw-bold mb-4"><?php the_title() ?></h3>
                        <?php the_excerpt(); ?>

                        <a href="<?php the_permalink() ?>" class="btn btn-secundario mt-2">Leia Mais</a>
                    </div>

                </div>
            </div>
            <?php
            $i++;
        endwhile; endif;
        wp_reset_postdata(); ?>
        <div class="row">
            <div class="col-12 pt-5 mt-5 text-center">
                <a href="#" class="btn btn-bordered">Ver Todas</a>
            </div>
        </div>
    </div>
</section>
