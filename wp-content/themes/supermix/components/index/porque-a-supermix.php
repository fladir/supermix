<?php
$grupoPqSupermix = get_field('porque_a_supermix');
#echo '<pre>'; print_r($grupoPqSupermix); echo '</pre>';
?>

<section id="porque-a-supermix">

    <div class="row">
        <div class="col-md-6 p-0">
            <img src="<?php print_r($grupoPqSupermix['imagem']['sizes']['col_6']) ?>"
                 alt="<?php echo $grupoPqSupermix['titulo'] ?>" title="<?php echo $grupoPqSupermix['titulo'] ?>">
        </div>

        <div class="col-md-6 conteudo"
             style="background-image: url(<?php echo get_template_directory_uri() . '/assets/img/background-supermix.jpg'; ?>)">
            <span class="traco"></span>
            <h2 class="mb-5"><?php echo $grupoPqSupermix['titulo'] ?></h2>
            <?php echo $grupoPqSupermix['texto'] ?>
            <a href="<?php echo $grupoPqSupermix['link_do_botao'] ?>" class="fw-medium"><?php echo $grupoPqSupermix['texto_do_botao'] ?><span class="undeline"></span></a>
        </div>
    </div>

</section>
