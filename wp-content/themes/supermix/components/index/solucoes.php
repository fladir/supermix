<?php $gruposolucoes = get_field('grupo_solucoes');
$solucoes = get_field('grupo_solucoes')['solucoes'];
?>
<?php //echo '<pre>';
//print_r($solucoes);
//echo '</pre>'; ?>
<section id="solucoes-home">
    <img class="fundo-solucoes" src="<?php echo get_template_directory_uri() . '/assets/img/fundo-coracao.png'; ?>" alt="">

    <div class="container">
        <div class="row">
            <div class="col-md-12 py-5">
                <h2 class="titulo-destaque text-center duplicate"
                    title="<?php echo $gruposolucoes['titulo']; ?>"><?php echo $gruposolucoes['titulo']; ?></h2>
            </div>
            <?php foreach ($solucoes as $solucao) : ?>
                <div class="col-md-4 card-solucao">
                    <a href="<?php echo $solucao['link']; ?>">
                        <div class="flip-card h-100">
                            <div class="flip-card-inner">
                                <div class="flip-card-front">
                                    <img src="<?php echo wp_get_attachment_image_url($solucao['imagem'], 'full'); ?>"
                                         alt="" class="mb-4">
                                    <h5 class="card-title"><?php echo $solucao['titulo']; ?></h5>
                                </div>
                                <div class="flip-card-back">
                                    <p><?php echo $solucao['texto']; ?></p>
                                </div>

                            </div>
                        </div>
                    </a>
                </div>
            <?php endforeach; ?>
            <div class="col-md-12 d-flex justify-content-center">
                <a href="<?php echo $gruposolucoes['link_do_botao']; ?>" class="btn btn-primario dark-hover text-center mt-5"><?php echo $gruposolucoes['texto_do_botao']; ?></a>
            </div>
        </div>
    </div>
</section>

