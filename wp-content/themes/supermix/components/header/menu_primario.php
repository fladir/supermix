<?php
$telefones = get_field('grupo_informacoes_para_contato', 'options')['telefones'];
$emails = get_field('grupo_informacoes_para_contato', 'options')['emails'];
$whatsapps = get_field('grupo_informacoes_para_contato', 'options')['whatsapp'];
$redesSociais = get_field('grupo_informacoes_para_contato', 'options')['redes_sociais'];
?>

<div id="menuPrimario" class="d-none d-lg-block">

    <div class="container">
        <div class="row align-items-center justify-content-around">
            <div class="col-6 text-left container contatos-topbar">
                <?php foreach ($telefones as $telefone) : ?>
                    <span class="telefone mr-3">
                                        <span class="icon-wrapper">
                                            <i class="fas fa-phone-alt mr-2 "></i>
                                        </span>
                                    <a href="tel:<?php echo $telefone['numero_telefone']; ?>" target="_blank">
                                    <?php echo $telefone['numero_telefone']; ?>
                                    </a>
                                </span>
                <?php endforeach; ?>

                <?php foreach ($whatsapps as $whatsapp) : ?>
                    <span class="whatsapp mr-3">
                                        <span class="icon-wrapper">
                                            <i class="fab fa-whatsapp mr-2"></i>
                                        </span>
                                    <a href="https://api.whatsapp.com/send?phone=55<?php echo $whatsapp['link_whatsapp']; ?>&text=Ola,%20tudo%20bem?"
                                       target="_blank">
                                    <?php echo $whatsapp['numero_whatsapp']; ?>
                                    </a>
                                </span>
                <?php endforeach; ?>

                <?php foreach ($emails as $email) : ?>
                    <span class="email mr-3">
                                        <span class="icon-wrapper">
                                            <i class="fas fa-envelope mr-2"></i>
                                        </span>
                                    <a href="mailto:<?php echo $email['endereco_email']; ?>" target="_blank">
                                    <?php echo $email['endereco_email']; ?>
                                    </a>
                                </span>
                <?php endforeach; ?>
            </div>

            <div class="col-6 text-right container">
                <?php foreach ($redesSociais as $redesSocial) : ?>
                    <span class="redes-sociais mr-1">
                                    <a href="mailto:<?php echo $redesSocial['link_social']; ?>" target="_blank"
                                       title="<?php echo $redesSocial['nome_rede_social']; ?>">
                                    <i class="<?php echo $redesSocial['icone_social']; ?>"></i>
                                    </a>
                                </span>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
