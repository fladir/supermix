<?php
$redesSociais = get_field('grupo_informacoes_para_contato', 'options')['redes_sociais']
?>
<div class="footer-copyright text-center py-3 container">


    <div class="container footer-copyright-content">

        <div class="row d-flex justify-content-between">
            <div class="col-md-5 py-2 d-flex justify-content-start">
                <span>
                    <i class="far fa-copyright"></i> <?php echo date('Y'); ?> Todos os direitos reservados. <?php echo get_bloginfo('name'); ?>
                </span>
            </div>
            <div class="col-md-3 py-2 d-flex justify-content-center">
                <span id="redes-sociais">
                        <?php foreach ($redesSociais as $redeSocial) : ?>
                            <a href="<?php echo $redeSocial['link_social'] ?>" target="_blank"
                               class="rede-social <?php echo $redeSocial['nome_rede_social'] ?>"
                               title="<?php echo $redeSocial['nome_rede_social'] ?>">
                            <i class="<?php echo $redeSocial['icone_social'] ?>"></i>
                        </a>
                        <?php endforeach; ?>
                </span>
            </div>

            <div class="col-md-4 py-2 d-flex justify-content-center justify-content-md-end">
                <a href="http://bit.ly/38DuDmd" target="_blank"> <img
                            src="<?php bloginfo('template_directory'); ?>/assets/img/logo-cia-branca.png"
                            class="logo-cia"/></a>
            </div>
        </div>

    </div>


</div>