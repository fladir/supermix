<?php
    function send_email(){
        $config = get_field('config_smtp','option');

        if(isset($_POST["securitycode"])){
            if (!$securitycode) {
                $errors = true;
            } else if (md5($securitycode) != $_SESSION['smartCheck']['securitycode']) {
                $errors = true;
            }
        }else{
            $errors = true;
        }

        if($errors){
            foreach ($_POST as $p) {
                if(validEmail($p)){
                    $senderemail = $p;
                    $sendername = explode('@',$senderemail)[0];
                }
            }

            require "phpmailer/PHPMailerAutoload.php";
            require "phpmailer/smartmessage.php";

            $receiver_email = $config['mail_do_destinatario'];
            $msg_subject = $_POST['msg_subject'];
            $order_file1 = uniqid();
            $order_upload1 = $order_file1.$_FILES['documento']['name'];

            if ($_FILES['documento']['error'] == 0) {
                move_uploaded_file($_FILES['documento']['tmp_name'], TEMPLATEPATH.'/includes/config-smtp/upload-emails/' . $order_upload1);
            }

            $mail = new PHPMailer();
            $mail->isSendmail();
            $mail->IsHTML(true);

            $mail->IsSMTP(); // Define que a mensagem será SMTP
            $mail->Host =  $config['host']; // Endereço do servidor SMTP (caso queira utilizar a autenticação, utilize o host smtp.seudomínio.com.br)
            $mail->SMTPAuth = true; // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
            $mail->Username =  $config['usuario_smtp']; // Usuário do servidor SMTP (endereço de email)
            $mail->Password =  $config['senha_smtp']; // Senha do servidor SMTP (senha do email usado)
            $mail->Port =  $config['porta_smtp']; // Senha do servidor SMTP (senha do email usado)
            $mail->SMTPSecure =  $config['seguranca_smtp']; // Senha do servidor SMTP (senha do email usado)
            $mail->SMTPDebug = 0;

            $mail->From = $config['usuario_smtp'];
            $mail->CharSet = "UTF-8";
            $mail->FromName = 'Site Vidraçaria São Cristóvão';
            $mail->Encoding = "base64";
            $mail->Timeout = 200;
            $mail->ContentType = "text/html";

            $mail->AddReplyTo($senderemail, $senderemail);

            if(isset($_POST['destinatario']) && $_POST['destinatario']){
                $mail->addAddress($_POST['destinatario'], $_POST['destinatario']);
            }else{
                $mail->addAddress($receiver_email, $receiver_email);
            }

//        if(isset($_POST['cc']) && $_POST['cc']){
//            $mail->addCC($_POST['cc'], $senderemail);
//        }

            if(isset($_POST['documento']) && $_POST['documento'] == 1) {
                $mail->AddAttachment(TEMPLATEPATH . '/includes/config-smtp/upload-emails/' . $order_upload1);
            }

            $mail->Subject = $msg_subject;
            $mail->Body = $message;
            $mail->AltBody = "Use an HTML compatible email client";

            if($mail->Send()) {
                if(isset($_POST['documento']) && $_POST['documento'] == true) {
                    $files = glob(TEMPLATEPATH.'/includes/config-smtp/upload-emails/*');
                    foreach($files as $file){
                        if(is_file($file))
                            unlink($file);
                    }
                }
            } else {
                echo '<div class="alert alert-danger">Falha ao enviar o email!<br></div>';
            }
        }else{
            echo '<div class="alert alert-danger">Falha ao enviar o email!<br></div>';
        }
        exit();
    }

    function validEmail($senderemail) {
        $isValid = true;
        $atIndex = strrpos($senderemail, "@");
        if (is_bool($atIndex) && !$atIndex) {
            $isValid = false;
        } else {
            $domain = substr($senderemail, $atIndex + 1);
            $local = substr($senderemail, 0, $atIndex);
            $localLen = strlen($local);
            $domainLen = strlen($domain);
            if ($localLen < 1 || $localLen > 64) {
                $isValid = false;
            } else if ($domainLen < 1 || $domainLen > 255) {
                $isValid = false;
            } else if ($local[0] == '.' || $local[$localLen - 1] == '.') {
                $isValid = false;
            } else if (preg_match('/\\.\\./', $local)) {
                $isValid = false;
            } else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain)) {
                $isValid = false;
            } else if (preg_match('/\\.\\./', $domain)) {
                $isValid = false;
            } else if (!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\", "", $local))) {
                if (!preg_match('/^"(\\\\"|[^"])+"$/', str_replace("\\\\", "", $local))) {
                    $isValid = false;
                }
            }
            if ($isValid && !(checkdnsrr($domain, "MX") || checkdnsrr($domain, "A"))) {
                $isValid = false;
            }
        }
        return $isValid;
    }

    add_action('wp_ajax_submit-contact-form', 'send_email');
    add_action('wp_ajax_nopriv_submit-contact-form', 'send_email');