<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'supemix' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'ciaserver_user' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', 'ciaserver_mysql' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'OnMLhr]#Ov6fSpT>dDoWw@I6s[NLkN^ggSFQzu3vDme`hOa|.A#]xKR%]aZGg?@T' );
define( 'SECURE_AUTH_KEY',  'Ikqr_q$$4qkhA-Pl~,~PEJ[h+NH04W9)*L%)IejixC?_8HrX[jR L#}!5}L*Du:_' );
define( 'LOGGED_IN_KEY',    '@XQ4$4Dm? vhym,:mQK|N+hBzn?X~5QP^Lyjup7H)z6C+J8eE!_]cy$hExHy.Qzt' );
define( 'NONCE_KEY',        ')TG=*o}qiiuS[g43@Y|F W g7+hNp@^3kf4+Y3L9jM2MmN%s5ivQ5gM*!tlprHkQ' );
define( 'AUTH_SALT',        '-|NFVAD|bXC_)#BY|beMwqv3<astTB;EQg;_j|%le9t N@QleCu*/9ucC^eK;t%V' );
define( 'SECURE_AUTH_SALT', 'zi?EQr5@tv>L9*syR[N[zn=MRBr&O&sYG(6^:caj$Rrkb9g``?%qD:ZECRJHOBuO' );
define( 'LOGGED_IN_SALT',   '=>[9l+jFk+9&b%jd+#.1b}q$t<A GxVldQ@h! ;]n>hwG*WFURCF7{dU$(9R{tmk' );
define( 'NONCE_SALT',       'hBG1Vc>7FW<LSmoKpx<<;S=/T[ums?S6F@s&Rl)2_=H?7{cR#G`3*yON=sUfvzBY' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';

/** Sets up 'direct' method for wordpress, auto update without ftp */
define('FS_METHOD','direct');

/** Altera a URL do site e ignora do banco de dados */
//define( 'WP_HOME', 'http://ciaserver/c/conexaoservicosempresariais.com.br/public/' );
//define( 'WP_SITEURL', 'http://ciaserver/c/conexaoservicosempresariais.com.br/public/' );